# BearMaps

## Synopsis

There are three parts to the project: Rastering, GraphDB, and Routing.

- The Rastering part takes as input an upper left latitude and longitude, a lower right latitude and longitude, a window width, and a window height. Using these six numbers and a Quadtree of images, we determine which set of images to display at what depth. 
![alt text](Rastering.png)

- The GraphDB class reads in the Open Street Map dataset and stores it as a graph. Each node in the graph represents a single intersection, and each edge represents a road.

- The Routing part takes in a GraphDB as input, a starting latitude and longitude, and a destination latitude and longitude, and it produces a list of nodes (i.e. intersections) that you get from the start point to the end point using the A* search algorithm and displays it.


## Code Examples

- Rastering:

```
public static ArrayList<QuadTree.Node> allDepthNodes(QueryBox qb, QuadTree.Node root, int depth, int currDepth,                                           ArrayList<QuadTree.Node> list) {
        if (currDepth > depth) {
            return null;
        }
        if (currDepth == depth) {
            list.add(root);
            return null;
        }
        if (qb.imagePointinQBox(qb, root.getnW1())) {
            allDepthNodes(qb, root.getnW1(), depth, currDepth + 1, list);
        }
        if (qb.imagePointinQBox(qb, root.getnE2())) {
            allDepthNodes(qb, root.getnE2(), depth, currDepth + 1, list);
        }
        if (qb.imagePointinQBox(qb, root.getsW3())) {
            allDepthNodes(qb, root.getsW3(), depth, currDepth + 1, list);
        }
        if (qb.imagePointinQBox(qb, root.getsE4())) {
            allDepthNodes(qb, root.getsE4(), depth, currDepth + 1, list);
        }
        return list;
}
```
- Routing:

```
SearchNode current;
pq.add(closestStart);
while (true) {
     current = pq.poll();
     if ((current.curr.lon == closestEnd.lon) && (current.curr.lat == closestEnd.lat)) {
         break;
     } else {
         result.add(current.curr);
     }
     for (Connection conn : current.curr.connectionSet) {
         for (GraphNode n : conn.list) {
              if (!result.contains(n)) {
                   double dist = 0;
                   SearchNode newCurr;
                   if (current.prev != null) {
                        newCurr = new SearchNode(n, current);
                   } else {
                        newCurr = new SearchNode(n, current);
                   }
                   newCurr.priority = newCurr.prev.priority + findEucDistance(current.curr.lat, current.curr.lon, newCurr.curr.lat, newCurr.curr.lon);
                   pq.add(newCurr);
                   newCurr.prev = current;
              }
          }
     }
     while (current != null) {
          path.addFirst(current.curr.id);
          current = current.prev;
     }
     route2 = path;
     return path;
}
```