public class SearchNode implements Comparable<SearchNode> {
    double priority;
    SearchNode prev;
    GraphNode curr;
//    double distTravelled;

    public SearchNode(GraphNode gNode) {
        curr = gNode;
        priority = 0;
//        distTravelled = 0;
    }

    public SearchNode(GraphNode gNode, SearchNode prevNode) {
        prev = prevNode;
        curr = gNode;
//        distTravelled = dt;
    }

    public int compareTo(SearchNode other) {
        if (this.priority == other.priority) {
            return 0;
        } else if (this.priority > other.priority) {
            return 1;
        } else {
            return -1;
        }
    }

}
