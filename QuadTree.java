/**
 * Created by pagarwal2015 on 4/9/16.
 */
public class QuadTree {
    private Node root;
    private static int depth;
    private int nDepth;

    public int getnDepth() {
        return nDepth;
    }

    public void setnDepth(int nDepth) {
        this.nDepth = nDepth;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public static int getDepth() {
        return depth;
    }

    public static void setDepth(int depth) {
        QuadTree.depth = depth;
    }


    public QuadTree(int depth) {
        this.depth = depth;
        nDepth = 0;
        root = new Node(MapServer.ROOT_ULLAT, MapServer.ROOT_ULLON,
                MapServer.ROOT_LRLAT, MapServer.ROOT_LRLON, "", nDepth, 0);
        root.children(this.depth);
    }

    public class Node {
        private double ulLat, ulLon, lrLat, lrLon;
        private Node nW1, nE2, sW3, sE4;
        private String imgName = "";
        private int nDepth;

        public String getImgName() {
            return imgName;
        }

        public void setImgName(String imgName) {
            this.imgName = imgName;
        }

        public double getUlLat() {
            return ulLat;
        }

        public void setUlLat(double ulLat) {
            this.ulLat = ulLat;
        }

        public double getUlLon() {
            return ulLon;
        }

        public void setUlLon(double ulLon) {
            this.ulLon = ulLon;
        }

        public double getLrLat() {
            return lrLat;
        }

        public void setLrLat(double lrLat) {
            this.lrLat = lrLat;
        }

        public double getLrLon() {
            return lrLon;
        }

        public void setLrLon(double lrLon) {
            this.lrLon = lrLon;
        }

        public Node getnW1() {
            return nW1;
        }

        public void setnW1(Node nW1) {
            this.nW1 = nW1;
        }

        public Node getnE2() {
            return nE2;
        }

        public void setnE2(Node nE2) {
            this.nE2 = nE2;
        }

        public Node getsW3() {
            return sW3;
        }

        public void setsW3(Node sW3) {
            this.sW3 = sW3;
        }

        public Node getsE4() {
            return sE4;
        }

        public void setsE4(Node sE4) {
            this.sE4 = sE4;
        }

        public int getnDepth() {
            return nDepth;
        }

        public void setnDepth(int nDepth) {
            this.nDepth = nDepth;
        }

        public Node(double ullat, double ullon, double lrlat, double lrlon,
                    String imgpName, int nDepth, int quadrant) {
            ulLat = ullat;
            ulLon = ullon;
            lrLat = lrlat;
            lrLon = lrlon;
            this.nDepth = nDepth;
            if (quadrant != 0) {
                imgName = imgpName + "" + quadrant;
            }
        }

        public void children(int dDepth) {
            if (nDepth == dDepth) {
                return;
            }
            double newLat = ((lrLat - ulLat) / 2) + ulLat;
            double newLon = ((lrLon - ulLon) / 2) + ulLon;
            nW1 = new Node(ulLat, ulLon, newLat, newLon, imgName, nDepth + 1, 1);
            nE2 = new Node(ulLat, newLon, newLat, lrLon, imgName, nDepth + 1, 2);
            sW3 = new Node(newLat, ulLon, lrLat, newLon, imgName, nDepth + 1, 3);
            sE4 = new Node(newLat, newLon, lrLat, lrLon, imgName, nDepth + 1, 4);
            nW1.children(dDepth);
            nE2.children(dDepth);
            sW3.children(dDepth);
            sE4.children(dDepth);

        }

    }

    public static void main(String[] args) {

    }

}

