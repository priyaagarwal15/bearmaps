import java.awt.Color;
import java.awt.Graphics;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.PriorityQueue;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.BasicStroke;

/* Maven is used to pull in these dependencies. */
import com.google.gson.Gson;

import static spark.Spark.*;

/**
 * This MapServer class is the entry point for running the JavaSpark web server for the BearMaps
 * application project, receiving API calls, handling the API call processing, and generating
 * requested images and routes.
 *
 * @author Alan Yao
 */
public class MapServer {
    /**
     * The root upper left/lower right longitudes and latitudes represent the bounding box of
     * the root tile, as the images in the img/ folder are scraped.
     * Longitude == x-axis; latitude == y-axis.
     */
    public static final double ROOT_ULLAT = 37.892195547244356, ROOT_ULLON = -122.2998046875,
            ROOT_LRLAT = 37.82280243352756, ROOT_LRLON = -122.2119140625;
    /**
     * Each tile is 256x256 pixels.
     */
    public static final int TILE_SIZE = 256;
    /**
     * HTTP failed response.
     */
    private static final int HALT_RESPONSE = 403;
    /**
     * Route stroke information: typically roads are not more than 5px wide.
     */
    public static final float ROUTE_STROKE_WIDTH_PX = 5.0f;
    /**
     * Route stroke information: Cyan with half transparency.
     */
    public static final Color ROUTE_STROKE_COLOR = new Color(108, 181, 230, 200);
    /**
     * The tile images are in the IMG_ROOT folder.
     */
    private static final String IMG_ROOT = "img/";
    /**
     * The OSM XML file path. Downloaded from <a href="http://download.bbbike.org/osm/">here</a>
     * using custom region selection.
     **/
    private static final String OSM_DB_PATH = "berkeley.osm";
    /**
     * Each raster request to the server will have the following parameters
     * as keys in the params map accessible by,
     * i.e., params.get("ullat") inside getMapRaster(). <br>
     * ullat -> upper left corner latitude,<br> ullon -> upper left corner longitude, <br>
     * lrlat -> lower right corner latitude,<br> lrlon -> lower right corner longitude <br>
     * w -> user viewport window width in pixels,<br> h -> user viewport height in pixels.
     **/
    private static final String[] REQUIRED_RASTER_REQUEST_PARAMS = {"ullat", "ullon", "lrlat",
        "lrlon", "w", "h"};
    /**
     * Each route request to the server will have the following parameters
     * as keys in the params map.<br>
     * start_lat -> start point latitude,<br> start_lon -> start point longitude,<br>
     * end_lat -> end point latitude, <br>end_lon -> end point longitude.
     **/
    private static final String[] REQUIRED_ROUTE_REQUEST_PARAMS = {"start_lat", "start_lon",
        "end_lat", "end_lon"};
    /* Define any static variables here. Do not define any instance variables of MapServer. */
    private static GraphDB g;
    private static LinkedList<Long> route2 = new LinkedList<>();

    /**
     * Validate & return a parameter map of the required request parameters.
     * Requires that all input parameters are doubles.
     *
     * @param req            HTTP Request
     * @param requiredParams TestParams to validate
     * @return A populated map of input parameter to it's numerical value.
     */
    private static HashMap<String, Double> getRequestParams(
            spark.Request req, String[] requiredParams) {
        //requiredParams = {"ullat, "ullon", "lrlat", "lrlon", "w", "h"}
        //queryParams takes in the Query string and extracts ullat, ullon, lrlat, lrlon, w, h
        Set<String> reqParams = req.queryParams();
        HashMap<String, Double> params = new HashMap<>();
        for (String param : requiredParams) {
            if (!reqParams.contains(param)) {
                halt(HALT_RESPONSE, "Request failed - parameters missing.");
            } else {
                try {
                    params.put(param, Double.parseDouble(req.queryParams(param)));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    halt(HALT_RESPONSE, "Incorrect parameters - provide numbers.");
                }
            }
        }
        return params;
    }


    /**
     * Handles raster API calls, queries for tiles and rasters the full image. <br>
     * <p>
     * The rastered photo must have the following properties:
     * <ul>
     * <li>Has dimensions of at least w by h, where w and h are the user viewport width
     * and height.</li>
     * <li>The tiles collected must cover the most longitudinal distance per pixel
     * possible, while still covering less than or equal to the amount of
     * longitudinal distance per pixel in the query box for the user viewport size. </li>
     * <li>Contains all tiles that intersect the query bounding box that fulfill the
     * above condition.</li>
     * <li>The tiles must be arranged in-order to reconstruct the full image.</li>
     * <li>If a current route exists, lines of width ROUTE_STROKE_WIDTH_PX and of color
     * ROUTE_STROKE_COLOR are drawn between all nodes on the route in the rastered photo.
     * </li>
     * </ul>
     * Additional image about the raster is returned and is to be included in the Json response.
     * </p>
     *
     * @param params Map of the HTTP GET request's query parameters - the query bounding box and
     *               the user viewport width and height.
     * @param os     An OutputStream that the resulting png image should be written to.
     * @return A map of parameters for the Json response as specified:
     * "raster_ul_lon" -> Double, the bounding upper left longitude of the rastered image <br>
     * "raster_ul_lat" -> Double, the bounding upper left latitude of the rastered image <br>
     * "raster_lr_lon" -> Double, the bounding lower right longitude of the rastered image <br>
     * "raster_lr_lat" -> Double, the bounding lower right latitude of the rastered image <br>
     * "raster_width"  -> Double, the width of the rastered image <br>
     * "raster_height" -> Double, the height of the rastered image <br>
     * "depth"         -> Double, the 1-indexed quadtree depth of the nodes of the rastered image.
     * Can also be interpreted as the length of the numbers in the image string. <br>
     * "query_success" -> Boolean, whether an image was successfully rastered. <br>
     * @see #REQUIRED_RASTER_REQUEST_PARAMS
     */
    public static Map<String, Object> getMapRaster(Map<String, Double> params,
                                                   OutputStream os) throws IOException {
        HashMap<String, Object> rasteredImageParams = new HashMap<>();
        QuadTree qt = new QuadTree(7);
        QueryBox queryBox = createBoxFromParams(params);
        ArrayList<QuadTree.Node> sortedNodes = new ArrayList<>();
        int depthToReach = findDepth(qt.getRoot(), queryBox, params, 0);
        sortedNodes = allDepthNodes(queryBox, qt.getRoot(), depthToReach, 0, sortedNodes);
        int width = calcBuffImageWidth(sortedNodes);
        int height = calcBuffImageHeight(sortedNodes);
        int nodeDepth = depthToReach;
        ArrayList<QuadTree.Node> orderedByLatNodes;
        orderedByLatNodes = sortedArrayList(sortedNodes);
        BufferedImage im = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g1 = im.getGraphics();
        int x = 0;
        int y = 0;
        for (QuadTree.Node image : orderedByLatNodes) {
            BufferedImage bi = ImageIO.read(new File(IMG_ROOT + image.getImgName() + ".png"));
            g1.drawImage(bi, x, y, null);
            x += 256;
            if (x >= im.getWidth()) {
                x = 0;
                y += bi.getHeight();
            }
        }
        int arraySize = orderedByLatNodes.size();
        rasteredImageParams.put("raster_ul_lon", orderedByLatNodes.get(0).getUlLon());
        rasteredImageParams.put("raster_ul_lat", orderedByLatNodes.get(0).getUlLat());
        rasteredImageParams.put("raster_lr_lon", orderedByLatNodes.get(arraySize - 1).getLrLon());
        rasteredImageParams.put("raster_lr_lat", orderedByLatNodes.get(arraySize - 1).getLrLat());
        rasteredImageParams.put("raster_width", width);
        rasteredImageParams.put("raster_height", height);
        rasteredImageParams.put("depth", nodeDepth);
        rasteredImageParams.put("query_success", true);

        double rasterUlLon = (double) rasteredImageParams.get("raster_ul_lon");
        double rasterLrLon = (double) rasteredImageParams.get("raster_lr_lon");
        double rasterUlLat = (double) rasteredImageParams.get("raster_ul_lat");
        double rasterLrLat = (double) rasteredImageParams.get("raster_lr_lat");
        int rasterWidth = (int) rasteredImageParams.get("raster_width");
        int rasterHeight = (int) rasteredImageParams.get("raster_height");
        double widthDPP = (rasterLrLon - rasterUlLon) / rasterWidth;
        double heightDPP = (rasterUlLat - rasterLrLat) / rasterHeight;
        GraphDB graph = new GraphDB("berkeley.osm");
        HashMap<Long, GraphNode> pathNodes = graph.pathNodes;
        Stroke stroke = new BasicStroke(MapServer.ROUTE_STROKE_WIDTH_PX,
                BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        ((Graphics2D) g1).setStroke(stroke);
        g1.setColor(MapServer.ROUTE_STROKE_COLOR);
        for (int i = 0; i < route2.size() - 1; i++) {
            Long first = route2.get(i);
            Long second = route2.get(i + 1);
            GraphNode firstNode = pathNodes.get(first);
            GraphNode secondNode = pathNodes.get(second);
            int startX = (int) ((firstNode.lon - rasterUlLon) / widthDPP);
            int startY = (int) ((rasterUlLat - firstNode.lat) / heightDPP);
            int endX = (int) ((secondNode.lon - rasterUlLon) / widthDPP);
            int endY = (int) ((rasterUlLat - secondNode.lat) / heightDPP);
            g1.drawLine(startX, startY, endX, endY);
        }

        ImageIO.write(im, "png", os);
        return rasteredImageParams;

        /*
        inputs: lon and lat
        goal: find the images
        */
    }

//    public static void testMethod() {
//        double lrLat = 37.87548268822065;
//        double lrLon = -122.24053369025242;
//        double ulLat = 37.87655856892288;
//        double ulLon = -122.24163047377972;
//        double queryDistancePerPixel = (122.24163047377972 - 122.24053369025242) / 892;
//        Map<String, Double> params = new HashMap<String, Double>();
//        params.put("ullon", ulLon);
//        params.put("lrlon", lrLon);
//        params.put("w", 892.0);
//        params.put("h", 875.0);
//        params.put("ullat", ulLat);
//        params.put("lrlat", lrLat);
//        //System.out.print(queryDistancePerPixel);
//        QuadTree test = new QuadTree(7);
//        QueryBox queryBox = new QueryBox(ulLat, ulLon, lrLat, lrLon);
//        //System.out.print(rect.getBounds2D());
//        QuadTree.Node quadrant = findNode(test.root, params, test.depth);
//        System.out.println(quadrant.imgName);
//        //System.out.print(test.getWidth());
//        //System.out.print(test.traverseForDepth(queryDistancePerPixel));
//        ArrayList<QuadTree.Node> newList = new ArrayList<>();
//        newList = allDepthNodes(queryBox, test.root, quadrant.nDepth, 0, newList);
//        int width = calcBuffImageWidth(newList);
//        int height = calcBuffImageHeight(newList);
//        ArrayList<QuadTree.Node> newList3 = new ArrayList<>();
//        newList3 = sortedArrayList(newList);
//        ArrayList<String> newList4 = new ArrayList<>();
//        newList4 = nodeToString(newList3);
//        System.out.println(newList4);
//    }

    public static QueryBox createBoxFromParams(Map<String, Double> params) {
        QueryBox qb = new QueryBox(params.get("ullat"), params.get("ullon"),
                params.get("lrlat"), params.get("lrlon"));
        return qb;
    }

    public static QuadTree.Node findNode(QuadTree.Node root,
                                         Map<String, Double> params, int depth) {
        QueryBox qb = createBoxFromParams(params);
        while (root.getnDepth() < depth) {
            if (qb.pointInImage(qb, root.getnW1().getUlLat(), root.getnW1().getUlLon(),
                    root.getnW1().getLrLat(), root.getnW1().getLrLon())) {
                if (qb.imageInQBox(qb, root.getnW1().getUlLat(), root.getnW1().getUlLon(),
                        root.getnW1().getLrLat(), root.getnW1().getLrLon(), params.get("w"))) {
                    return root.getnW1();
                } else {
                    return findNode(root.getnW1(), params, depth);
                }
            } else if (qb.pointInImage(qb, root.getnE2().getUlLat(), root.getnE2().getUlLon(),
                    root.getnE2().getLrLat(), root.getnE2().getLrLon())) {
                if (qb.imageInQBox(qb, root.getnE2().getUlLat(), root.getnE2().getUlLon(),
                        root.getnE2().getLrLat(), root.getnE2().getLrLon(), params.get("w"))) {
                    return root.getnE2();
                } else {
                    return findNode(root.getnE2(), params, depth);
                }
            } else if (qb.pointInImage(qb, root.getsW3().getUlLat(), root.getsW3().getUlLon(),
                    root.getsW3().getLrLat(), root.getsW3().getLrLon())) {
                if (qb.imageInQBox(qb, root.getsW3().getUlLat(), root.getsW3().getUlLon(),
                        root.getsW3().getLrLat(), root.getsW3().getLrLon(), params.get("w"))) {
                    return root.getsW3();
                } else {
                    return findNode(root.getsW3(), params, depth);
                }
            } else if (qb.pointInImage(qb, root.getsE4().getUlLat(), root.getsE4().getUlLon(),
                    root.getsE4().getLrLat(), root.getsE4().getLrLon())) {
                if (qb.imageInQBox(qb, root.getsE4().getUlLat(), root.getsE4().getUlLon(),
                        root.getsE4().getLrLat(), root.getsE4().getLrLon(), params.get("w"))) {
                    return root.getsE4();
                } else {
                    return findNode(root.getsE4(), params, depth);
                }
            } else {
                return root;
            }
        }
        return root;
    }

    public static double nodePDensity(QuadTree.Node root) {
        double nodeLonDiff = (root.getLrLon() - root.getUlLon()) / 256;
        return nodeLonDiff;
    }

    public static double qbPDensity(QueryBox qb, Map<String, Double> params) {
        double qbLonDiff = (qb.getLrLon() - qb.getUlLon()) / params.get("w");
        return qbLonDiff;
    }

    public static int findDepth(QuadTree.Node root, QueryBox qb, Map<String,
            Double> params, int count) {
        if (nodePDensity(root) <= qbPDensity(qb, params)) {
            return count;
        } else if (count >= 7) {
            return 7;
        } else {
            return findDepth(root.getnW1(), qb, params, count + 1);
        }
    }

    public static ArrayList<QuadTree.Node> allDepthNodes(QueryBox qb,
                                                         QuadTree.Node root, int depth,
                                                         int currDepth,
                                                         ArrayList<QuadTree.Node> list) {
        if (currDepth > depth) {
            return null;
        }
        if (currDepth == depth) {
            list.add(root);
            return null;
        }
        if (qb.imagePointinQBox(qb, root.getnW1())) {
            allDepthNodes(qb, root.getnW1(), depth, currDepth + 1, list);
        }
        if (qb.imagePointinQBox(qb, root.getnE2())) {
            allDepthNodes(qb, root.getnE2(), depth, currDepth + 1, list);
        }
        if (qb.imagePointinQBox(qb, root.getsW3())) {
            allDepthNodes(qb, root.getsW3(), depth, currDepth + 1, list);
        }
        if (qb.imagePointinQBox(qb, root.getsE4())) {
            allDepthNodes(qb, root.getsE4(), depth, currDepth + 1, list);
        }
        return list;
    }

    public static ArrayList<QuadTree.Node>
        buffImageWidthArray(ArrayList<QuadTree.Node> sortedNodes) {
        double mainLat = sortedNodes.get(0).getUlLat();
        ArrayList<QuadTree.Node> sameWidth = new ArrayList<>();
        for (int i = 0; i < sortedNodes.size(); i++) {
            if ((sortedNodes.get(i).getUlLat()) == (mainLat)) {
                sameWidth.add(sortedNodes.get(i));
            }
        }
        return sameWidth;

    }

    public static int calcBuffImageTileNumber(ArrayList<QuadTree.Node> sortedNodes) {
        return buffImageWidthArray(sortedNodes).size();
    }

    public static int calcBuffImageWidth(ArrayList<QuadTree.Node> sortedNodes) {
        return (calcBuffImageTileNumber(sortedNodes) * 256);
    }

    public static int calcBuffImageHeight(ArrayList<QuadTree.Node> sortedNodes) {
        return (sortedNodes.size() / (calcBuffImageTileNumber(sortedNodes)) * 256);
    }

    public static ArrayList<QuadTree.Node> sortedArrayList(ArrayList<QuadTree.Node> sortedNodes) {
        ArrayList<Double> nodeLats = new ArrayList<>();
        for (int i = 0; i < sortedNodes.size(); i++) {
            nodeLats.add(sortedNodes.get(i).getUlLat());
        }
        LinkedHashSet<Double> sortLats = new LinkedHashSet<>();
        sortLats.addAll(nodeLats);
        nodeLats.clear();
        nodeLats.addAll(sortLats);
        ArrayList<QuadTree.Node> order = new ArrayList<>();
        for (int j = 0; j < nodeLats.size(); j++) {
            for (int k = 0; k < sortedNodes.size(); k++) {
                if (sortedNodes.get(k).getUlLat() == nodeLats.get(j)) {
                    order.add(sortedNodes.get(k));
                }
            }
        }
        return order;
    }

    public static ArrayList<String> nodeToString(ArrayList<QuadTree.Node> order) {
        ArrayList<String> stringArray = new ArrayList<>();
        for (int i = 0; i < order.size(); i++) {
            stringArray.add(order.get(i).getImgName() + ".png");
        }
        return stringArray;
    }


    /**
     * Searches for the shortest route satisfying the input request parameters, sets it to be the
     * current route, and returns a <code>LinkedList</code> of the route's node ids for testing
     * purposes. <br>
     * The route should start from the closest node to the start point and end at the closest node
     * to the endpoint. Distance is defined as the euclidean between two points (lon1, lat1) and
     * (lon2, lat2).
     *
     * @param params from the API call described in REQUIRED_ROUTE_REQUEST_PARAMS
     * @return A LinkedList of node ids from the start of the route to the end.
     */
    public static LinkedList<Long> findAndSetRoute(Map<String, Double> params) {
        HashMap<Long, GraphNode> pathNodes = g.pathNodes;
        SearchNode closestStart = null;
        GraphNode closestEnd = null;
        PriorityQueue<SearchNode> pq = new PriorityQueue<>();
        double latStart = params.get("start_lat");
        double lonStart = params.get("start_lon");
        double latEnd = params.get("end_lat");
        double lonEnd = params.get("end_lon");
        double distance1 = Double.MAX_VALUE;
        double distance2 = Double.MAX_VALUE;
        HashSet<GraphNode> result = new HashSet<>();
        LinkedList<Long> path = new LinkedList<>();
        for (GraphNode gNode : pathNodes.values()) {
            if (findEucDistance(latStart, lonStart, gNode.lat, gNode.lon) < distance1) {
                distance1 = findEucDistance(latStart, lonStart, gNode.lat, gNode.lon);
                closestStart = new SearchNode(gNode, null);
            }
            if (findEucDistance(latEnd, lonEnd, gNode.lat, gNode.lon) < distance2) {
                distance2 = findEucDistance(latEnd, lonEnd, gNode.lat, gNode.lon);
                closestEnd = gNode;
            }
        }
        SearchNode current;
        pq.add(closestStart);
        while (true) {
            current = pq.poll();
            if ((current.curr.lon == closestEnd.lon) && (current.curr.lat == closestEnd.lat)) {
                break;
            } else {
                result.add(current.curr);
            }
            for (Connection conn : current.curr.connectionSet) {
                for (GraphNode n : conn.list) {
                    if (!result.contains(n)) {
                        double dist = 0;
                        SearchNode newCurr;
                        if (current.prev != null) {
                            newCurr = new SearchNode(n, current);
                        } else {
                            newCurr = new SearchNode(n, current);
                        }
                        newCurr.priority = newCurr.prev.priority
                                + findEucDistance(current.curr.lat, current.curr.lon,
                                        newCurr.curr.lat, newCurr.curr.lon);
                        pq.add(newCurr);
                        newCurr.prev = current;
                    }
                }
            }
        }
        while (current != null) {
            path.addFirst(current.curr.id);
            current = current.prev;
        }
        route2 = path;
        return path;
    }

    public static double findEucDistance(double startlat, double startlon,
                                         double endlat, double endlon) {
        return Math.sqrt(Math.pow(startlon - endlon, 2) + Math.pow(startlat - endlat, 2));
    }

    /**
     * Clear the current found route, if it exists.
     */
    public static void clearRoute() {
        route2 = new LinkedList<>();
    }

    /**
     * In linear time, collect all the names of OSM locations that prefix-match the query string.
     *
     * @param prefix Prefix string to be searched for. Could be any case, with our without
     *               punctuation.
     * @return A <code>List</code> of the full names of locations whose cleaned name matches the
     * cleaned <code>prefix</code>.
     */
    public static List<String> getLocationsByPrefix(String prefix) {
        return new LinkedList<>();
    }

    /**
     * Collect all locations that match a cleaned <code>locationName</code>, and return
     * information about each node that matches.
     *
     * @param locationName A full name of a location searched for.
     * @return A list of locations whose cleaned name matches the
     * cleaned <code>locationName</code>, and each location is a map of parameters for the Json
     * response as specified: <br>
     * "lat" -> Number, The latitude of the node. <br>
     * "lon" -> Number, The longitude of the node. <br>
     * "name" -> String, The actual name of the node. <br>
     * "id" -> Number, The id of the node. <br>
     */
    public static List<Map<String, Object>> getLocations(String locationName) {
        return new LinkedList<>();
    }

    /**
     * Place any initialization statements that will be run before the server main loop here.
     * Do not place it in the main function. Do not place initialization code anywhere else.
     * This is for testing purposes, and you may fail tests otherwise.
     **/
    public static void initialize() {
        g = new GraphDB(OSM_DB_PATH);
    }

    public static void main(String[] args) {
        initialize();
        staticFileLocation("/page");
        /* Allow for all origin requests (since this is not an authenticated server, we do not
         * care about CSRF).  */
        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Request-Method", "*");
            response.header("Access-Control-Allow-Headers", "*");
        });

        /* Define the raster endpoint for HTTP GET requests. I use anonymous functions to define
         * the request handlers. */
        get("/raster", (req, res) -> {
            HashMap<String, Double> params =
                    getRequestParams(req, REQUIRED_RASTER_REQUEST_PARAMS);
            /* The png image is written to the ByteArrayOutputStream */
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            /* getMapRaster() does almost all the work for this API call */
            Map<String, Object> rasteredImgParams = getMapRaster(params, os);
            /* On an image query success, add the image data to the response */
            if (rasteredImgParams.containsKey("query_success")
                    && (Boolean) rasteredImgParams.get("query_success")) {
                String encodedImage = Base64.getEncoder().encodeToString(os.toByteArray());
                rasteredImgParams.put("b64_encoded_image_data", encodedImage);
            }
            /* Encode response to Json */
            Gson gson = new Gson();
            return gson.toJson(rasteredImgParams);
        });

        /* Define the routing endpoint for HTTP GET requests. */
        get("/route", (req, res) -> {
            HashMap<String, Double> params =
                    getRequestParams(req, REQUIRED_ROUTE_REQUEST_PARAMS);
            LinkedList<Long> route = findAndSetRoute(params);
            return !route.isEmpty();
        });

        /* Define the API endpoint for clearing the current route. */
        get("/clear_route", (req, res) -> {
            clearRoute();
            return true;
        });

        /* Define the API endpoint for search */
        get("/search", (req, res) -> {
            Set<String> reqParams = req.queryParams();
            String term = req.queryParams("term");
            Gson gson = new Gson();
            /* Search for actual location data. */
            if (reqParams.contains("full")) {
                List<Map<String, Object>> data = getLocations(term);
                return gson.toJson(data);
            } else {
                /* Search for prefix matching strings. */
                List<String> matches = getLocationsByPrefix(term);
                return gson.toJson(matches);
            }
        });

        /* Define map application redirect */
        get("/", (request, response) -> {
            response.redirect("/map.html", 301);
            return true;
        });

//        testMethod();
    }

}

/**
 * To sort my ArrayList:
 * Sources:
 * http://beginnersbook.com/2013/12/how-to-sort-arraylist-in-java/
 * http://beginnersbook.com/2014/10/how-to-remove-repeated-elements-from-arraylist/
 */
