/**
 * Created by pagarwal2015 on 4/13/16.
 */
public class QueryBox {
    private double ulLat, ulLon, lrLat, lrLon;
    private double qbwidth;
    private double qbheight;

    public double getQbheight() {
        return qbheight;
    }

    public void setQbheight(double qbheight) {
        this.qbheight = qbheight;
    }

    public double getQbwidth() {
        return qbwidth;
    }

    public void setQbwidth(double qbwidth) {
        this.qbwidth = qbwidth;
    }

    public double getLrLon() {
        return lrLon;
    }

    public void setLrLon(double lrLon) {
        this.lrLon = lrLon;
    }

    public double getLrLat() {
        return lrLat;
    }

    public void setLrLat(double lrLat) {
        this.lrLat = lrLat;
    }

    public double getUlLon() {
        return ulLon;
    }

    public void setUlLon(double ulLon) {
        this.ulLon = ulLon;
    }

    public double getUlLat() {
        return ulLat;
    }

    public void setUlLat(double ulLat) {
        this.ulLat = ulLat;
    }

    public QueryBox(double ullat, double ullon, double lrlat, double lrlon) {
        ulLat = ullat;
        ulLon = ullon;
        lrLat = lrlat;
        lrLon = lrlon;
        qbwidth = Math.abs(lrLon - ulLon);
        qbheight = Math.abs(lrLat - ulLat);

    }

    public boolean pointInImage(QueryBox qb, double imageLLat, double imageLLon,
                                double imageRLat, double imageRLon) {
        double qbCenterLat = ((qb.lrLat + qb.ulLat) / 2);
        double qbCenterLon = ((qb.lrLon + qb.ulLon) / 2);
        return ((qbCenterLat <= imageLLat) && (qbCenterLat >= imageRLat)
                && (qbCenterLon >= imageLLon) && (qbCenterLon <= imageRLon));
    }

    public boolean imageInQBox(QueryBox qb, double imageLLat, double imageLLon,
                               double imageRLat, double imageRLon, double boxWidth) {
        double imageCalcWidth = (imageRLon - imageLLon) / 256;
        double qbCalcWidth = (qb.lrLon - qb.ulLon) / boxWidth;
        return ((qb.ulLat <= imageLLat) && (qb.lrLat >= imageRLat) && (qb.ulLon >= imageLLon)
                && (qb.lrLon <= imageRLon) && (imageCalcWidth <= qbCalcWidth));
    }

    public boolean imagePointinQBox(QueryBox qb, QuadTree.Node root) {
        return ((root.getLrLon() > qb.ulLon) && (root.getUlLon() < qb.lrLon)
                && (root.getUlLat() > qb.lrLat) && (root.getLrLat() < qb.ulLat));
    }
}
