import java.util.ArrayList;

/**
 * Created by pagarwal2015 on 4/16/16.
 */
public class Connection {
    ArrayList<GraphNode> list;

    public Connection() {
        list = new ArrayList<>();

    }

    public void addToList(GraphNode node) {
        list.add(node);
    }

    public void updateNode() {
        for (int i = 0; i < list.size() - 1; i++) {
            GraphNode firstNode = list.get(i);
            GraphNode secondNode = list.get(i + 1);
            Connection conn = new Connection();
            conn.addToList(firstNode);
            conn.addToList(secondNode);
            firstNode.connectionSet.add(conn);
        }
        for (int j = list.size() - 1; j > 0; j--) {
            GraphNode firstNode2 = list.get(j);
            GraphNode secondNode2 = list.get(j - 1);
            Connection conn2 = new Connection();
            conn2.addToList(firstNode2);
            conn2.addToList(secondNode2);
            firstNode2.connectionSet.add(conn2);
        }
    }
}
