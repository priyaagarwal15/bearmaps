import java.util.HashSet;
/**
 * Created by pagarwal2015 on 4/16/16.
 */
public class GraphNode {
    double lon;
    double lat;
    long id;
    HashSet<Connection> connectionSet;

    public GraphNode(double longitude, double latitude, long iden) {
        lon = longitude;
        lat = latitude;
        id = iden;
        connectionSet = new HashSet<>();


    }
}
